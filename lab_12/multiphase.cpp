#include <iostream>
#include <fstream>
#include <iostream>


void main() {
    const int s = 4;
    char mass[s];
    int ss = 0;
    int k;
    int chet = 0;
    char N;
    std::ifstream fin("numbers.txt");
    std::ofstream afile("A.txt");
    std::ofstream bfile("B.txt");

    //чтение и запись в файлы
    if (fin.is_open())//если файл открыт
    {

        while (!fin.eof())//пока не достигнут конец файла
        {
            if (ss < 4) 
            {
                k = 0;
                while (k < s)//(нужно чтобы брать по 4 числа из файла)
                {
                    fin >> N;
                    mass[k] = N;
                    k++;
                }
                for (int i = 0; i < s; i++) //сортируем т что прочитали 
                {
                    for (int j = i + 1; j < s; j++) 
                    {
                        if (mass[i] > mass[j]) 
                        {
                            char tmp;
                            tmp = mass[i];
                            mass[i] = mass[j];
                            mass[j] = tmp;
                        }
                    }
                }
                if (chet % 2 == 0)//если остаток от деления на 2 = 0 то запись в файл аа 
                {
                    for (int i = 0; i < s; i++) 
                    {
                        afile << mass[i];
                    }
                }
                else //иначе запись в файл bb
                {
                    for (int i = 0; i < s; i++) 
                    {
                        bfile << mass[i];
                    }
                }
                chet++;
                ss++;;
            }
            else break;
        }

    }
    fin.close();//закрыть все три файла
    afile.close();
    bfile.close();


    k = 0;
    std::ofstream cfile("C.txt");
    std::ofstream dfile("D.txt");

    //открываем ранее созданные файлы с записанными частями массива
    std::ifstream AFile("A.txt");
    std::ifstream BFile("B.txt");


    char mas[s];
    char ms[s];
    int d = 0;

    while (!AFile.eof() && !BFile.eof()) //пока не достигнут конец файлов
    {
        if (d < 2) 
        {
            k = 0;
            while (k < s)//запись из одного файла в первый массив
            {
                AFile >> N;
                mas[k++] = N;
            }
            k = 0;
            while (k < s)//запись из второго файла вовторой массив
            {
                BFile >> N;
                ms[k++] = N;
            }

            int a = 0;
            int b = 0;
            int g = 0;

            //этап сравнения и сортировки элементов массива из файла и их запись в другие файлы в отсортированном виде
            if (d % 4 == 0 || d % 4 == 2) //если остаток 0 или 2 РАБОТА С ФАЙЛОМ С
            {
                while (g < 2 * s) //s = 4 
                {

                    if (a < s && b < s) 
                    {
                        if (mas[a] > ms[b]) //если а-тый больше б-го то запись б-го элемента в ранее созданый и пока ещё пустой файл С 
                        {
                            cfile << ms[b];
                            b++;
                        }
                        else //иначе а-того элемента
                        {
                            cfile << mas[a];
                            a++;
                        }
                    }
                    else //иначе(если какой-то из элементов >= s(4) )
                    {
                        if (a < s) 
                        {
                            cfile << mas[a];
                            a++;
                        }
                        else if (b < s) 
                        {
                            cfile << ms[b];
                            b++;
                        }
                    }
                    g++;
                }
            }
            else if (d % 4 == 1 || d % 4 == 3) //если остаток 1 или 3 РАБОТА С ФАЙЛОМ D
            {
                while (g < 2 * s) 
                {

                    if (a < s && b < s) 
                    {
                        if (mas[a] > ms[b]) 
                        {
                            dfile << ms[b];
                            b++;
                        }
                        else 
                        {
                            dfile << mas[a];
                            a++;
                        }
                    }
                    else 
                    {
                        if (a < s) 
                        {
                            dfile << mas[a];
                            a++;
                        }
                        else if (b < s) 
                        {
                            dfile << ms[b];
                            b++;
                        }
                    }
                    g++;
                }
            }
            d++;//
        }
        else
            break;
    }


    cfile.close();
    dfile.close();

    int e = 0;

    std::ifstream CFile("C.txt");
    std::ifstream DFile("D.txt");

    std::ofstream exitFile("Exit.txt");

    char masi[2 * s];
    char ma[2 * s];

    //точно такой же этап сортировки элементов и их запись в итоговый файл
    while (!DFile.eof() && !CFile.eof()) 
    {
        if (e < 1) 
        {
            k = 0;
            while (k < 2 * s)
            {
                CFile >> N;
                masi[k++] = N;
            }
            k = 0;
            while (k < 2 * s)
            {
                DFile >> N;
                ma[k++] = N;
            }

            int a = 0;
            int b = 0;
            int g = 0;

            while (g < 4 * s) //сортировка и запись в конечный файл
            {

                if (a < 2 * s && b < 2 * s) 
                {
                    if (masi[a] > ma[b]) 
                    {
                        exitFile << ma[b];
                        b++;
                    }
                    else 
                    {
                        exitFile << masi[a];
                        a++;
                    }
                }
                else 
                {
                    if (a < 2 * s) 
                    {
                        exitFile << masi[a];
                        a++;
                    }
                    else if (b < 2 * s) 
                    {
                        exitFile << ma[b];
                        b++;
                    }
                }
                g++;
            }

        }
        else
            break;

        e++;
    }

    AFile.close();
    BFile.close();
    CFile.close();
    DFile.close();
}