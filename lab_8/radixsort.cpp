#include <iostream>

#define N 10
#define MAX_DIGIT 4 //digit �����, number �����


int minusSize(int number, int size)
{
    while (size > 1)
    {
        number /= 10;
        size--;
    }
    return number % 10;
}

void radixSort(int dop_mas[N][N], int mas[N], int size)
{
    int mas_string[N], temp = 0;
    for (int i = 0; i < N; i++) //�������� ��� ��������
        mas_string[i] = 0;//
    for (int i = 0; i < N; i++) //��� �� ��������� ������� ������� ���������
    {
        int j = minusSize(mas[i], size);// ����������� j ����������� �� �������� ������� 
        dop_mas[mas_string[j]][j] = mas[i];//����� �� ������� �� �� ���� �� ������� mas_string �� ����� 0 
        mas_string[j]++;//����������� ��� �������� �� ��������� ������
    }
    for (int i = 0; i < N; i++)
        for (int k = 0; k < mas_string[i]; k++)
        {
            mas[temp] = dop_mas[k][i];
            temp++;
        }
}

int main()
{

    int mas[N] = { 100, 3, 456, 784, 254, 64, 30, 1238, 6542, 1239 };
    int dop_mas[N][N];
    for (int size = 1; size < MAX_DIGIT + 1; size++)
        radixSort(dop_mas, mas, size);
    for (int i = 0; i < N; i++)
        std::cout << mas[i] << " " ;
    return 0;
}