#include <iostream>
#include <chrono>

#define N 10

void QuickSort(int a, int b, int* mas)
{
	if (a >= b)
		return;
	int m = ((rand() * rand()) % (b - a + 1)) + a;
	int k = mas[m];
	int l = a - 1;                           //����� �������
	int r = b + 1;                          //������ ������� 
	while (1)
	{
		do
		{
			l += 1;
		} while (mas[l] < k);
		do
		{
			r -= 1;
		} while (mas[r] > k);

		if (l >= r)
			break;
		std::swap(mas[r], mas[l]);
	}
	r = l;
	l = l - 1;
	QuickSort(a, l, mas);
	QuickSort(r, b, mas);
}

int main()
{
	srand(time(0));
	int* mass = new int[N]{};

	for (int i = 0; i < N; i++)
	{
		mass[i] = rand();
	}

	for (int i = 0; i < N; i++)
		std::cout << mass[i] << " ";
	std::cout << " " << '\n';

	QuickSort(0, N - 1, mass);

	for (int i = 0; i < N; i++)
		std::cout << mass[i] << " ";

	delete[] mass;
}