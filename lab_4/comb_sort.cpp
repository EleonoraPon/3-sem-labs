#include <iostream>

#define N 10

void CombSort(int* mas)
{
    double coef = 1.3;
    int step = N - 1;
    
    while (step >= 1)
    {
        for (int i = 0; i + step < N; i++)
        {
            if(mas[i] > mas[i + step])
            {
               std::swap(mas[i], mas[i + step]); 
            }
        }
        step = step/coef;
    }
    
    
}

int main()
{
	srand(time(0));
	
	int* mass = new int[N]{4, 2, 7, 9, 3, 6, 1, 13, 98, 5};

	//for (int i = 0; i < N; i++)
	//	mass[i] = rand();
	for (int i = 0; i < N; i++)
		std::cout << mass[i] << " ";
	std::cout << " " << '\n';
    
    CombSort(mass);
	

	for (int i = 0; i < N; i++)
		std::cout << mass[i] << " ";

	delete[] mass;
}
