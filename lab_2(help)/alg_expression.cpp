#include <iostream>
#include <string>

std::string calculation(std::string expr)//подсчёт результата выражения, лучше после поверки 
{
    int a = 0;
    int b = 0;
    int c = 0;
    int y = 1;
    int count1 = 1;
    int count2 = 1;

    for (int i = 0; i < expr.length(); i++)
    {
        if (expr[i] == ')')
        {
            for (int j = i - 1; j > -1; j--) 
            {
                if (expr[j] == '(')
                {
                    std::string dop;
                    for (int foo = j + 1; foo < i; foo++) 
                    {
                        dop.push_back(expr[foo]);
                    }
                    dop = calculation(dop);
                    expr.erase(j, i - j + 1);
                    expr.insert(j, dop);
                    dop.clear();

                    i = 0;
                    j = -2;
                }
            }
        }
    }
    for (int i = 0; i < expr.length(); i++)
    {
        if (expr[i] == '*' || expr[i] == '/')
        {
            for (int j = i - 1; j > -1; j--) 
            {
                if (expr[j] == '1' || expr[j] == '2' || expr[j] == '1' || expr[j] == '3' || expr[j] == '4' || expr[j] == '5' || expr[j] == '6' 
                    || expr[j] == '7' || expr[j] == '8' || expr[j] == '9' || expr[j] == '0')
                {
                    a = a + ((int)expr[j] - 48) * y;
                    y = y * 10;
                    count2 = j;
                }
                else 
                {
                    y = 1;
                    j = -2;
                }
            }
            for (int j = i + 1; j < expr.length(); j++)
            {
                if (expr[j] == '1' || expr[j] == '2' || expr[j] == '1' || expr[j] == '3' || expr[j] == '4' || expr[j] == '5' || expr[j] == '6'
                    || expr[j] == '7' || expr[j] == '8' || expr[j] == '9' || expr[j] == '0')
                {
                    b = b * 10 + ((int)expr[j] - 48);
                    count1 = j;
                }
                else 
                {
                    j = expr.length() + 1;
                }
            }
            if (expr[i] == '*')
                c = a * b;
            if (expr[i] == '/')
                c = a / b;

            std::string cc = std::to_string(c);
            expr.erase(count2, count1 - count2 + 1);
            expr.insert(count2, cc);
            a = 0;
            b = 0;
            c = 0;
            y = 1;
            count1 = 1;
            count2 = 1;
            i = 0;
        }
    }

    for (int i = 0; i < expr.length(); i++)
    {
        if (expr[i] == '+' || expr[i] == '-')
        {
            for (int j = i - 1; j > -1; j--) 
            {
                if (expr[j] == '1' || expr[j] == '2' || expr[j] == '1' || expr[j] == '3' || expr[j] == '4' || expr[j] == '5' || expr[j] == '6' 
                    || expr[j] == '7' || expr[j] == '8' || expr[j] == '9' || expr[j] == '0')
                {
                    a = a + ((int)expr[j] - 48) * y;
                    y = y * 10;
                    count2 = j;
                }
                else 
                {
                    y = 1;
                    j = -2;
                }
            }
            for (int j = i + 1; j < expr.length(); j++)
            {
                if (expr[j] == '1' || expr[j] == '2' || expr[j] == '1' || expr[j] == '3' || expr[j] == '4' || expr[j] == '5' || expr[j] == '6' 
                    || expr[j] == '7' || expr[j] == '8' || expr[j] == '9' || expr[j] == '0')
                {
                    b = b * 10 + ((int)expr[j] - 48);
                    count1 = j;
                }
                else
                    j = expr.length() + 1;
            }
            if (expr[i] == '+')
                c = a + b;
            if (expr[i] == '-')
                c = a - b;

            std::string cc = std::to_string(c);
            expr.erase(count2, count1 - count2 + 1);
            expr.insert(count2, cc);
            a = 0;
            b = 0;
            c = 0;
            y = 1;
            count1 = 1;
            count2 = 1;
            i = 0;
        }
    }
    return(expr);
}



int examination(std::string expr) //задача проверить наличие парильной расстановки скобок, знаков и наличия знака = и знаков в целом
{                                //в нашем случае а должно схранить значение 0 и это будет значить 
    int a = 0;                   //что в выражении правильная последовательность скобок
    if (expr[expr.length() - 1] != '=')
    {
        a++;
        return (a);
    }
    if (expr[0] != '(' && expr[0] != '0' && expr[0] != '1' && expr[0] != '2' && expr[0] != '3' && expr[0] != '4' && expr[0] != '5' && expr[0] != '6' 
        && expr[0] != '7' && expr[0] != '8' && expr[0] != '9') 
    {
        a++;
        return (a);
    }
    for (int i = 1; i < expr.length() - 1; i++)
    {
        if (expr[i] == '+' || expr[i] == '-' || expr[i] == '*' || expr[i] == '/')
        {   //если не цифра то а не 0 => выражение неверно и такаая проверка далее в if де содержится "!"
            if (!(expr[i + 1] == '1' || expr[i + 1] == '2' || expr[i + 1] == '3' || expr[i + 1] == '4' || expr[i + 1] == '5' || expr[i + 1] == '6' 
                || expr[i + 1] == '7' || expr[i + 1] == '8' || expr[i + 1] == '9' || expr[i + 1] == '0' || expr[i + 1] == '('))
            {
                a++;
                return (a);
            }
            
            if (!(expr[i - 1] == '1' || expr[i - 1] == '2' || expr[i - 1] == '3' || expr[i - 1] == '4' || expr[i - 1] == '5' || expr[i - 1] == '6' 
                || expr[i - 1] == '7' || expr[i - 1] == '8' || expr[i - 1] == '9' || expr[i - 1] == '0' || expr[i - 1] == ')'))
            {
                a++;
                return (a);
            }
            
        }
        if (expr[i] == '(')
        {
            if (!(expr[i + 1] == '1' || expr[i + 1] == '2' || expr[i + 1] == '3' || expr[i + 1] == '4' || expr[i + 1] == '5' || expr[i + 1] == '6' 
                || expr[i + 1] == '7' || expr[i + 1] == '8' || expr[i + 1] == '9' || expr[i + 1] == '0' || expr[i + 1] == '('))
            {
                a++;
                return (a);
            }
            
            if (!(expr[i - 1] == '+' || expr[i - 1] == '-' || expr[i - 1] == '*' || expr[i - 1] == '/' || expr[i - 1] == '('))
            {
                a++;
                return (a);
            }
            
        }
        if (expr[i] == ')')
        {
            if (!(expr[i - 1] == '1' || expr[i - 1] == '2' || expr[i - 1] == '3' || expr[i - 1] == '4' || expr[i - 1] == '5' || expr[i - 1] == '6' 
                || expr[i - 1] == '7' || expr[i - 1] == '8' || expr[i - 1] == '9' || expr[i - 1] == '0' || expr[i - 1] == ')'))
            {
                a++;
                return (a);
            }
            
            if (!(expr[i + 1] == '+' || expr[i + 1] == '-' || expr[i + 1] == '*' || expr[i + 1] == '/' || expr[i + 1] == ')' || expr[i + 1] == '='))
            {
                a++;
                return (a);
            }

        }
        if (expr[i] == '/')
        {
            if (expr[i] == '0')
            {
                a++;
                return (a);
            }
        }
    }

    for (int i = 0; i < expr.length(); i++) //
    {
        int opening = 0;
        int  closed = 0;
        if (expr[i] == '(')
            opening++;

        if (expr[i] == ')')
            closed++;

        a = opening + closed;
    }
    return (a);
}




int main()
{
    setlocale(LC_ALL, "Rus");

    std::string expression;
    std::cin >> expression;
    int a = examination(expression);//сначала проверяем

    if (a == 0) //(т.е. если пример прошел проверку на правильную последовательность скобок)
    {
        std::cout << expression;
        expression = calculation(expression);//потом считаем
        expression.erase(expression.length() - 1, 1);
        std::cout << expression;
    }
    else 
        std::cout << "выражение введено неверно";

    return 0;
}