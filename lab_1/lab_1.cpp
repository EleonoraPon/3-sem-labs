﻿#include <iostream>
#include <string>
#include <stack>


int main()
{
    setlocale(LC_ALL, "Rus");

    std::stack <char> subsequence;// стэк для скобок
    std::string line_subque; //строка скобок 
    std::cout << "введите последовательность (subsequence)" << std::endl;
    std::cin >> line_subque;

    for (int i = 0; i < line_subque.length(); i++)
    {
        char current = line_subque[i];
        if (current == '(' || current == '[' || current == '{')
            subsequence.push(current);
        else if (subsequence.empty())//если не будет этой проверки то в случае когда в конце последовательности будет 
                          //одна из закрывающих скобок, программа в попытке получить вершину стека (в которой ничегот нет) упадёт
        {
            subsequence.push(current);
            break;
        }
        else if (current == ')')
        {
            if (subsequence.top() == '(') 
                subsequence.pop();
            else
                break;
        }
        else if (current == ']')
        {
            if (subsequence.top() == '[')
                subsequence.pop();
            else
                break;
        }
        else if (current == '}')
        {
            if (subsequence.top() == '{')
                subsequence.pop();
            else
                break;
        }
    }

    if (!subsequence.empty())
        std::cout << "скобочная последовательность неправильная" << std::endl;
    else 
        std::cout << "скобочная последовательность правильная" << std::endl;

    return 0;
}
