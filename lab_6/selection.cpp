#include <iostream>

#define N 10



void Selection(int* mas)
{
    for(int i = 0; i <= N - 1; i++)
    {
        int j;
        int jMin = i;
        for(j = i + 1; j <= N; j++)
        {
            if(mas[j] < mas[jMin])
            {
                jMin = j;
            }
        }
        if(jMin != j )
        {
            std::swap(mas[i], mas[jMin]);
        }
    }
}


int main()
{
	
	int* mass = new int[N]{4, 2, 7, 9, 3, 6, 1, 13, 98, 5};

	for (int i = 0; i < N; i++)
		std::cout << mass[i] << " ";
	std::cout << " " << '\n';
    
    Selection(mass);

	for (int i = 0; i < N; i++)
		std::cout << mass[i] << " ";

	delete[] mass;
}
