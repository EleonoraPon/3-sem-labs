#include <iostream>
#include <Windows.h>
#include<vector>
#include<string>

static std::string ch_hor = "-", ch_ver = "|", ch_ddia = "/", ch_rddia = "\\", ch_udia = "\\", ch_ver_hor = "|-", ch_udia_hor = "\\-", ch_ddia_hor = "/-", ch_ver_spa = "| ";

struct elem {
    int dat;//data - данные
    //указатели на соседние вершины
    elem* l;
    elem* r;
    elem* p;
};





struct elem* ADD(struct elem* rt, std::string& s) //структура добавления дерева
{



    if (s[0] == '(' && s[1] != ',' && s.length() > 2) 
    {
        s.erase(0, 1);
        rt->l = (struct elem*)malloc(sizeof(struct elem));
        rt->l->dat = (int)s[0] - 48;
        rt->l->l = rt->l->r = NULL;
        rt->l->p = rt;
        s.erase(0, 1);
        rt->l = ADD(rt->l, s);
    }



    if (s[0] == ',' && s.length() > 2) 
    {

        s.erase(0, 1);
        if (rt->p != NULL) 
        {
            rt->p->r = (struct elem*)malloc(sizeof(struct elem));
            rt->p->r->dat = (int)s[0] - 48;
            rt->p->r->l = rt->p->r->r = NULL;
            rt->p->r->p = rt->p;
            s.erase(0, 1);
            rt->p->r = ADD(rt->p->r, s);
        }
        else 
        {
            rt->r = (struct elem*)malloc(sizeof(struct elem));
            rt->r->dat = (int)s[0] - 48;
            rt->r->l = rt->r->r = NULL;
            rt->r->p = rt;
            s.erase(0, 1);
            rt->r = ADD(rt->r, s);
        }

    }
    if ((s[0] == ')' || s[0] == '(' && s[1] == ',') && s.length() > 2) 
    {
        s.erase(0, 1);
        if (rt->p != NULL) 
        {
            rt->p = ADD(rt->p, s);
        }
        else 
        {
            rt = ADD(rt, s);
        }
    }


    return rt;
}


void prim(elem* t)
{
    if (t == NULL) return;

    std::cout << t->dat << " ";

    prim(t->l);
    prim(t->r);
}

void centr(struct elem* root) 
{
    if (root != NULL) 
    {
        if (root->l != NULL)
            centr(root->l);
        std::cout << root->dat << " ";
        if (root->r != NULL)
            centr(root->r);

    }

}

void konz(struct elem* root) 
{
    if (root != NULL) 
    {
        if (root->l != NULL && root->dat >= 0)
            konz(root->l);
        if (root->r != NULL && root->dat >= 0)
            konz(root->r);
        if (root->dat >= 0)
            std::cout << root->dat << " ";
    }
}


struct elem* ADD(int x, elem* tr) // добавление дерева
{
    if (tr == NULL) 
    { // Если дерева нет, то формируем корень
        tr = new elem; // память под узел
        tr->dat = x;   // поле данных
        tr->l = NULL;
        tr->r = NULL; // ветви инициализируем пустотой
    }
    else  if (x < tr->dat)   // условие добавление левого потомка
        tr->l = ADD(x, tr->l);
    else    // условие добавление правого потомка
        tr->r = ADD(x, tr->r);
    return(tr);
}


int Poisk(elem* rt, int k) 
{
    int j = 1212;
    if (rt != NULL) 
    {
        if (rt->dat == k) 
        {
            j = k;
            return j;
        }
        else 
        {
            if (rt->l != NULL && j != k)
                j = Poisk(rt->l, k);
            if (rt->r != NULL && j != k)
                j = Poisk(rt->r, k);
        }

    }
    return j;
}



void dump(elem const* node, bool high = true, std::vector<std::string> const& lpref = std::vector<std::string>(), 
    std::vector<std::string> const& cpref = std::vector<std::string>(), std::vector<std::string> const& rpref = std::vector<std::string>(), 
    bool root = true, bool left = true, std::shared_ptr<std::vector<std::vector<std::string>>> lines = nullptr) 
{
    if (!node) return;
    typedef std::vector<std::string> VS;
    auto VSCat = [](VS const& a, VS const& b) { auto r = a; r.insert(r.end(), b.begin(), b.end()); return r; };
    if (root) lines = std::make_shared<std::vector<VS>>();
    if (node->l)
        dump(node->l, high, VSCat(lpref, high ? VS({ " ", " " }) : VS({ " " })), VSCat(lpref, high ? VS({ ch_ddia, ch_ver }) : VS({ ch_ddia })), VSCat(lpref, high ? VS({ ch_hor, " " }) : VS({ ch_hor })), false, true, lines);
    auto sval = std::to_string(node->dat);
    size_t sm = left || sval.empty() ? sval.size() / 2 : ((sval.size() + 1) / 2 - 1);
    for (size_t i = 0; i < sval.size(); ++i)
        lines->push_back(VSCat(i < sm ? lpref : i == sm ? cpref : rpref, { std::string(1, sval[i]) }));
    if (node->r)
        dump(node->r, high, VSCat(rpref, high ? VS({ ch_hor, " " }) : VS({ ch_hor })), VSCat(rpref, high ? VS({ ch_rddia, ch_ver }) : VS({ ch_rddia })), VSCat(rpref, high ? VS({ " ", " " }) : VS({ " " })), false, false, lines);
    if (root) 
    {
        VS out;
        for (size_t l = 0;; ++l) 
        {
            bool last = true;
            std::string line;
            for (size_t i = 0; i < lines->size(); ++i)
                if (l < (*lines).at(i).size()) 
                {
                    line += lines->at(i)[l];
                    last = false;
                }
                else line += " ";
            if (last) break;
            out.push_back(line);
        }
        for (size_t i = 0; i < out.size(); ++i)
            std::cout << out[i] << std::endl;
    }
}




elem* DeleteNode(elem* node, int val) 
{
    if (node == NULL)
        return node;

    if (val == node->dat) 
    {

        elem* tmp;
        if (node->r == NULL)
            tmp = node->l;
        else 
        {

            elem* ptr = node->r;
            if (ptr->l == NULL) 
            {
                ptr->l = node->l;
                tmp = ptr;
            }
            else 
            {
                elem* pmin = ptr->l;
                while (pmin->l != NULL) 
                {
                    ptr = pmin;
                    pmin = ptr->l;
                }
                ptr->l = pmin->r;
                pmin->l = node->l;
                pmin->r = node->r;
                tmp = pmin;
            }
        }

        delete node;
        return tmp;
    }
    else if (val != node->dat)
        node->l = DeleteNode(node->l, val);
    else
        node->r = DeleteNode(node->r, val);
    return node;
}




int main()
{
    HANDLE hConsoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsoleHandle, FOREGROUND_GREEN);
    setlocale(LC_ALL, "Russian");

    std::string tx;
    std::cout << "Введите дерево в формате линейно-скобочной записи" << std::endl;
    std::cin >> tx;
    struct elem* root;
    root = NULL;

    root = (struct elem*)malloc(sizeof(struct elem));
    root-> dat = (int)tx[0] - 48;
    root->l = root->r = root->p = NULL;

    if (tx.length() > 1) 
    {
        tx.erase(0, 1);

    }

    if (tx.length() > 1) 
    {
        root = ADD(root, tx);
    }

    while (1) 
    {
        std::cout << std::endl;
        std::cout << "для поиска числа нажмите 1" << std::endl;
        std::cout << "Для удаления ветви нажмите 2" << std::endl;
        std::cout << "Для добавления числа нажмите 3" << std::endl;
        std::cout << "Для завершения программы нажмите 4" << std::endl;
        int condition;//условие
        std::cin >> condition;


        if (condition == 1) 
        {
            std::cout << "Введите число" << std::endl;
            int rrr;
            std::cin >> rrr;
            int y = Poisk(root, rrr);
            if (y == 1212) 
            {
                std::cout << "Такого числа нет" << std::endl;
            }
            else
            {
                std::cout << "Такое число есть" << std::endl;
            }
        }
        else if (condition == 2) 
        {
            std::cout << "Введите число, с которого начинается ветвь" << std::endl;
            int yyy;
            std::cin >> yyy;
            root = DeleteNode(root, yyy);


        }
        else if (condition == 3) 
        {
            std::cout << "Введите число" << std::endl;
            int jj;
            std::cin >> jj;
            root = ADD(jj, root);

        }
        else if (condition == 4) 
        {
            std::cout << std::endl;
            //prim(root);
            dump(root);
            break;
        }
    }
}