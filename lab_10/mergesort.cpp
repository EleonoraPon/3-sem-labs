#include <iostream>

#define N 10

void Merge(int a, int b, int* mas)
{
	if (a == b)
		return;

	int m = (a + b) / 2;
	Merge(a, m, mas);
	Merge(m + 1, b, mas);
	int i = a;
	int j = m + 1;
	int pop[N];
	for (int k = a; k <= b; k++)
	{
		if ((j > b) || (i <= m) && (mas[i]) < mas[j])
		{
			pop[k] = mas[i];
			i++;
		}
		else
		{
			pop[k] = mas[j];
			j++;
		}
	}
	for (int i = a; i <= b; i++)
		mas[i] = pop[i];
}

int main()
{
	srand(time(0));
	
	int* mass = new int[N]{4, 2, 7, 9, 3, 6, 1, 13, 98, 5};

	//for (int i = 0; i < N; i++)
	//	mass[i] = rand();
	for (int i = 0; i < N; i++)
		std::cout << mass[i] << " ";
	std::cout << " " << '\n';

	Merge(0, N-1, mass);       //�-1 �� ��� ���� ���������� ������������� 6-7������� ����� 

	for (int i = 0; i < N; i++)
		std::cout << mass[i] << " ";

	delete[] mass;
}