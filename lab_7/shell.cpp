#include <iostream>

#define N 10



void Shell(int* mas)
{
    int h = N/2;
    while(h > 0)
    {
        for(int i = h; i < N; i++)
        {
            int x = mas[i];
            int j = i - h;
            while(j >= 0 && x < mas[j])
            {
                mas[j + h] = mas[j];
                j = j - h;
            }
            mas[j + h] = x;
        }
        h = h/2;
    }
}


int main()
{
	
	int* mass = new int[N]{4, 2, 7, 9, 3, 6, 1, 13, 98, 5};

	for (int i = 0; i < N; i++)
		std::cout << mass[i] << " ";
	std::cout << " " << '\n';
    
    Shell(mass);

	for (int i = 0; i < N; i++)
		std::cout << mass[i] << " ";

	delete[] mass;
}
