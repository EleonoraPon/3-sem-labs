#include <iostream>

#define N 10

//������ - "������� ���������" (�������� � �������� ����������)
void binaryHeap(int arr[], int n, int i)// i- ������ � ������ � �������, � - ������ ����
{
    int largest = i; // �������������� ���������� ������� ��� ������
    int l = 2 * i + 1; // ����� 
    int r = 2 * i + 2; // ������ 

    if (l < n && arr[l] > arr[largest])// ���� ����� ������� ������ �����
        largest = l;

    if (r < n && arr[r] > arr[largest])// ���� ������ ������� ������, ��� ����� �������
        largest = r;

    if (largest != i)// ���� ����� ������� ������� �� ������
    {
        std::swap(arr[i], arr[largest]);

        binaryHeap (arr, n, largest);
    }
}


void Heap(int arr[], int n)
{
    for (int i = n / 2 - 1; i >= 0; i--)
        binaryHeap(arr, n, i);// ���������� ���� 

    for (int i = n - 1; i >= 0; i--)//��������� ������� �� ���� (�� ������)
    {
        std::swap(arr[0], arr[i]);// ���������� ������� ������ � �����

        binaryHeap(arr, i, 0); 
    }
}

int main()
{
	int* mass = new int[N]{4, 2, 7, 9, 3, 6, 1, 13, 98, 5};

	for (int i = 0; i < N; i++)
		std::cout << mass[i] << " ";
	std::cout << " " << '\n';

	Heap(mass, N);

	for (int i = 0; i < N; i++)
		std::cout << mass[i] << " ";

	delete[] mass;
}